# Bash Script SSL Certificate Generator

This bash script generates SSL certificate signing requests (CSR) for the specified domain.

## Prerequisites

- OpenSSL

## Usage

1. Edit the script by replacing the following placeholders with your desired values:

    - `THEDOMAIN`: The domain for which you want to generate the CSR.
    - `COUNTRY`: The country code of the organization.
    - `STATE`: The state where the organization is located.
    - `CITY`: The city where the organization is located.
    - `COMPANY`: The name of the company.
    - `DEPARTMENT`: The department within the company.

2. Run the script using the following command:

    ```bash
    ./ssl-cert-generator.sh
    ```

## Script Details

The script performs the following steps:

1. Prints information about the system and configuration.
2. Creates a directory for the domain if it doesn't exist.
3. Changes to the domain directory.
4. Generates the CSR using OpenSSL with the specified information.

**Note:** This script assumes that you have OpenSSL installed on your system.

For any support or assistance, please contact the following email address: [admin@thedomain.com](mailto:admin@thedomain.com).