#!/bin/bash
#edit this then just run ./generate-csr
THEDOMAIN=*.changeme.com
COUNTRY=US
STATE=Colorado
CITY=Denver
COMPANY="The Company, LLC"
DEPARTMENT="Web"

#DONT MONKEY WITH THE MONKEY
########################################
#ssl csr generator
THISDUR=`pwd`
DONE=`date +%Y-%m-%d`
MAILADDR=admin@thedomain.com
red='\e[0;31m'
RED='\e[1;31m'
blue='\e[0;34m'
BLUE='\e[1;34m'
cyan='\e[0;36m'
CYAN='\e[1;36m'
NC='\e[0m'
#######################################

echo -e "##############################################################################################"
echo -e "#       ${CYAN}THE COMPANY NETWORKS                                                      ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "${NC}# ${BLUE} Patch       ${RED}:${BLUE} null                                           ${NC}"
echo -e "${NC}# ${BLUE} Server      ${RED}:${BLUE} $HOSTNAME                                      ${NC}"
echo -e "${NC}# ${BLUE} Date        ${RED}:${BLUE} $DONE                                          ${NC}"
echo -e "${NC}# ${BLUE} Kernel      ${RED}:${BLUE} `uname -r`                                     ${NC}"
echo -e "${NC}# ${BLUE} Architecture${RED}:${BLUE} `uname -m`                                     ${NC}"
echo -e "${NC}# ${BLUE} Processor   ${RED}:${BLUE} `uname -p`                                     ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "${NC}# ${BLUE}Support                                                                    ${NC}"
echo -e "${NC}# ${BLUE}  Email     ${RED}: ${BLUE}MAILADDR                                        ${NC}"
echo -e "${NC}# ${BLUE}  Server URL${RED}: ${BLUE}http://`uname -n`                               ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "#                                                                                        ${NC}"
echo -e "${NC}#########################################################################################"
sleep 2

if [ -d ./$THEDOMAIN ] ; then
echo "domain exists $THEDOMAIN, now entering directory...."
cd $THEDOMAIN
openssl req -nodes -newkey rsa:2048 -nodes -keyout $THEDOMAIN.key -out $THEDOMAIN.csr \
-subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$COMPANY/OU=$DEPARTMENT/CN=$THEDOMAIN"
else
mkdir $THEDOMAIN
cd $THEDOMAIN
echo "made new domain dir $THEDOMAIN"
openssl req -nodes -newkey rsa:2048 -nodes -keyout $THEDOMAIN.key -out $THEDOMAIN.csr \
-subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$COMPANY/OU=$DEPARTMENT/CN=$THEDOMAIN"
fi